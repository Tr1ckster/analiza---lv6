﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Analiza___Zadatak_2
{
    public partial class Form1 : Form
    {
        private string path = "C:\\Users\\Vuki\\Desktop\\OOP\\vjesalo.txt";

        private string izabran = "";
        private string skriven = "";

        private List<string> lista = new List<string>();
        private Random rng = new Random();
        private int broj_Pokusaja;

        public Form1()
        {
            InitializeComponent();
        }

        public void Reset()
        {
            izabran = lista[rng.Next(0, lista.Count - 1)];
            broj_Pokusaja = 5;
            textBox1.Clear();
            label6.Text = broj_Pokusaja.ToString();
            label5.Text = string.Empty;
            skriven = "";
            for (int i = 0; i < izabran.Length; i++)
            {
                skriven += "_";
            }
            follow();
        }

        public void follow()
        {
            label3.Text = "";
            for (int i = 0; i < skriven.Length; i++)
            {
                label3.Text += skriven.Substring(i, 1);
                label3.Text += " ";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    lista.Add(line);
                }
                Reset();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length == 1)
            {
                if (izabran.Contains(textBox1.Text))
                {
                    char[] temp = skriven.ToCharArray();
                    char[] find = izabran.ToCharArray();
                    char charac = textBox1.Text.ElementAt(0);
                    for (int i = 0; i < find.Length; i++)
                    {
                        if (find[i] == charac)
                        {
                            temp[i] = charac;
                        }
                    }
                    skriven = new string(temp);
                    follow();

                    label5.Text += textBox1.Text;
                    label5.Text += " ";
                    textBox1.Clear();

                }
                else
                {
                    broj_Pokusaja--;
                    label5.Text += textBox1.Text;
                    label5.Text += " ";
                    textBox1.Clear();
                    label6.Text = broj_Pokusaja.ToString();
                    if (broj_Pokusaja == 0)
                    {
                        MessageBox.Show("OBJESEN!!");                        
                    }
                }
            }
            else if (textBox1.Text == izabran)
            {
                textBox1.Clear();
                MessageBox.Show("POBJEDA!!");
            }
            else
            {
                broj_Pokusaja--;
                textBox1.Clear();
                if (broj_Pokusaja == 0)
                {
                    MessageBox.Show(string.Format("OBJESEN!!"));                   
                }
                label6.Text = broj_Pokusaja.ToString();
            }
        }
    }
}
