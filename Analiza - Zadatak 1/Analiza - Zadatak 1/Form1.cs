﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Analiza___Zadatak_1
{
    public partial class Form1 : Form
    {
        double x, y, rez;

        public Form1()
        {
            InitializeComponent();
        }

        private void puta_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tbx.Text, out x))
            {
                MessageBox.Show("Pogreška");
            }
            if (!double.TryParse(tby.Text, out y))
            {
                MessageBox.Show("Pogreška");
            }
            rez = x * y;
            tbrez.Text = rez.ToString();
            tbrez.Show();
        }

        private void podijeljeno_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tbx.Text, out x))
            {
                MessageBox.Show("Pogreška");
            }
            if (!double.TryParse(tby.Text, out y))
            {
                MessageBox.Show("Pogreška");
            }
            rez = x / y;
            tbrez.Text = rez.ToString();
            tbrez.Show();
        }

        private void sin_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tbx.Text, out x))
            {
                MessageBox.Show("Pogreška");
            }
            rez = Math.Sin(x);
            tbrez.Text = rez.ToString();
            tbrez.Show();
        }

        private void cos_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tbx.Text, out x))
            {
                MessageBox.Show("Pogreška");
            }
            rez = Math.Cos(x);
            tbrez.Text = rez.ToString();
            tbrez.Show();
        }

        private void kvadrat_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tbx.Text, out x))
            {
                MessageBox.Show("Pogreška");
            }
            rez = x*x;
            tbrez.Text = rez.ToString();
            tbrez.Show();
        }

        private void Korijen_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tbx.Text, out x))
            {
                MessageBox.Show("Pogreška");
            }
            rez = Math.Sqrt(x);
            tbrez.Text = rez.ToString();
            tbrez.Show();
        }

        private void log_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tbx.Text, out x))
            {
                MessageBox.Show("Pogreška");
            }
            rez = Math.Log(x);
            tbrez.Text = rez.ToString();
            tbrez.Show();
        }

        private void obrisi_Click(object sender, EventArgs e)
        {
            tbx.Clear();
            tby.Clear();
        }

        private void plus_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tbx.Text, out x))
            {
                MessageBox.Show("Pogreška");
            }
            if (!double.TryParse(tby.Text, out y))
            {
                MessageBox.Show("Pogreška");
            }
            rez = x + y;
            tbrez.Text = rez.ToString();
            tbrez.Show();
        }

        private void minus_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tbx.Text, out x))
            {
                MessageBox.Show("Pogreška");
            }
            if (!double.TryParse(tby.Text, out y))
            {
                MessageBox.Show("Pogreška");
            }
            rez = x - y;
            tbrez.Text = rez.ToString();
            tbrez.Show();
        }
    }
}
