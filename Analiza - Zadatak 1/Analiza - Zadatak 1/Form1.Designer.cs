﻿namespace Analiza___Zadatak_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tbx = new System.Windows.Forms.TextBox();
            this.tby = new System.Windows.Forms.TextBox();
            this.tbrez = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.kvadrat = new System.Windows.Forms.Button();
            this.Korijen = new System.Windows.Forms.Button();
            this.obrisi = new System.Windows.Forms.Button();
            this.log = new System.Windows.Forms.Button();
            this.plus = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.puta = new System.Windows.Forms.Button();
            this.podijeljeno = new System.Windows.Forms.Button();
            this.cos = new System.Windows.Forms.Button();
            this.sin = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Prvi broj (glavni)";
            // 
            // tbx
            // 
            this.tbx.Location = new System.Drawing.Point(15, 29);
            this.tbx.Name = "tbx";
            this.tbx.Size = new System.Drawing.Size(156, 22);
            this.tbx.TabIndex = 1;
            // 
            // tby
            // 
            this.tby.Location = new System.Drawing.Point(15, 94);
            this.tby.Name = "tby";
            this.tby.Size = new System.Drawing.Size(156, 22);
            this.tby.TabIndex = 2;
            // 
            // tbrez
            // 
            this.tbrez.Location = new System.Drawing.Point(15, 152);
            this.tbrez.Name = "tbrez";
            this.tbrez.Size = new System.Drawing.Size(156, 22);
            this.tbrez.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Drugi broj";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Rezultat";
            // 
            // kvadrat
            // 
            this.kvadrat.Location = new System.Drawing.Point(177, 180);
            this.kvadrat.Margin = new System.Windows.Forms.Padding(5);
            this.kvadrat.Name = "kvadrat";
            this.kvadrat.Size = new System.Drawing.Size(75, 50);
            this.kvadrat.TabIndex = 6;
            this.kvadrat.Text = "Kvadrat";
            this.kvadrat.UseVisualStyleBackColor = true;
            this.kvadrat.Click += new System.EventHandler(this.kvadrat_Click);
            // 
            // Korijen
            // 
            this.Korijen.Location = new System.Drawing.Point(258, 180);
            this.Korijen.Name = "Korijen";
            this.Korijen.Size = new System.Drawing.Size(75, 50);
            this.Korijen.TabIndex = 7;
            this.Korijen.Text = "Korijen";
            this.Korijen.UseVisualStyleBackColor = true;
            this.Korijen.Click += new System.EventHandler(this.Korijen_Click);
            // 
            // obrisi
            // 
            this.obrisi.Location = new System.Drawing.Point(177, 124);
            this.obrisi.Name = "obrisi";
            this.obrisi.Size = new System.Drawing.Size(75, 50);
            this.obrisi.TabIndex = 8;
            this.obrisi.Text = "Obrisi";
            this.obrisi.UseVisualStyleBackColor = true;
            this.obrisi.Click += new System.EventHandler(this.obrisi_Click);
            // 
            // log
            // 
            this.log.Location = new System.Drawing.Point(258, 124);
            this.log.Name = "log";
            this.log.Size = new System.Drawing.Size(75, 50);
            this.log.TabIndex = 9;
            this.log.Text = "log";
            this.log.UseVisualStyleBackColor = true;
            this.log.Click += new System.EventHandler(this.log_Click);
            // 
            // plus
            // 
            this.plus.Location = new System.Drawing.Point(177, 12);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(75, 50);
            this.plus.TabIndex = 10;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.plus_Click);
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(258, 12);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(75, 50);
            this.minus.TabIndex = 11;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // puta
            // 
            this.puta.Location = new System.Drawing.Point(177, 68);
            this.puta.Name = "puta";
            this.puta.Size = new System.Drawing.Size(75, 50);
            this.puta.TabIndex = 12;
            this.puta.Text = "*";
            this.puta.UseVisualStyleBackColor = true;
            this.puta.Click += new System.EventHandler(this.puta_Click);
            // 
            // podijeljeno
            // 
            this.podijeljeno.Location = new System.Drawing.Point(258, 68);
            this.podijeljeno.Name = "podijeljeno";
            this.podijeljeno.Size = new System.Drawing.Size(75, 48);
            this.podijeljeno.TabIndex = 13;
            this.podijeljeno.Text = "/";
            this.podijeljeno.UseVisualStyleBackColor = true;
            this.podijeljeno.Click += new System.EventHandler(this.podijeljeno_Click);
            // 
            // cos
            // 
            this.cos.Location = new System.Drawing.Point(96, 180);
            this.cos.Name = "cos";
            this.cos.Size = new System.Drawing.Size(75, 50);
            this.cos.TabIndex = 14;
            this.cos.Text = "cos";
            this.cos.UseVisualStyleBackColor = true;
            this.cos.Click += new System.EventHandler(this.cos_Click);
            // 
            // sin
            // 
            this.sin.Location = new System.Drawing.Point(15, 180);
            this.sin.Name = "sin";
            this.sin.Size = new System.Drawing.Size(75, 50);
            this.sin.TabIndex = 15;
            this.sin.Text = "sin";
            this.sin.UseVisualStyleBackColor = true;
            this.sin.Click += new System.EventHandler(this.sin_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 252);
            this.Controls.Add(this.sin);
            this.Controls.Add(this.cos);
            this.Controls.Add(this.podijeljeno);
            this.Controls.Add(this.puta);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.log);
            this.Controls.Add(this.obrisi);
            this.Controls.Add(this.Korijen);
            this.Controls.Add(this.kvadrat);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbrez);
            this.Controls.Add(this.tby);
            this.Controls.Add(this.tbx);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbx;
        private System.Windows.Forms.TextBox tby;
        private System.Windows.Forms.TextBox tbrez;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button kvadrat;
        private System.Windows.Forms.Button Korijen;
        private System.Windows.Forms.Button obrisi;
        private System.Windows.Forms.Button log;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button puta;
        private System.Windows.Forms.Button podijeljeno;
        private System.Windows.Forms.Button cos;
        private System.Windows.Forms.Button sin;
    }
}

